FROM gem5:latest

RUN apt-get update
RUN apt-get install -y subversion gcc-arm-linux-gnueabihf autoconf libtool
RUN apt-get clean

# Instalando o wolfssl.
WORKDIR /usr/local/src
RUN git clone https://github.com/wolfssl/wolfssl.git
WORKDIR /usr/local/src/wolfssl
# Removendo a pasta git pois ela leva a erros na configuração/compilação da biblioteca
# RUN rm -rf .git
RUN ./autogen.sh
RUN ./configure --host=arm-linux CC=arm-linux-gnueabihf-gcc LD=arm-linux-gnueabihf-ld AR=arm-linux-gnueabihf-ar RANLIB=arm-linux-gnueabihf-ranlib --prefix='/usr/arm-linux-gnueabihf/' --enable-static --enable-sha3 --enable-sha512 --enable-ecc --enable-ecccustcurves --enable-eccencrypt CFLAGS=-DHAVE_ECC_KOBLITZ CPPFLAGS="-I./"
RUN make
RUN make install
RUN ldconfig

WORKDIR /usr/local/src/gem5
RUN svn export https://llvm.org/svn/llvm-project/test-suite/tags/RELEASE_390/final/SingleSource/Benchmarks/Stanford/ se-benchmark

ENTRYPOINT bash

# armebv7-eabihf-glibc  ./configure --host=armeb-linux CC=armeb-linux-gcc LD=armeb-linux-ld AR=armeb-linux-ar RANLIB=armeb-linux-ranlib CFLAGS="-DWOLFSSL_USER_IO -Os" CPPFLAGS="-I./"
# armv6-eabihf-glibc    ./configure --host=arm-linux CC=arm-linux-gcc LD=arm-linux-ld AR=arm-linux-ar RANLIB=arm-linux-ranlib CFLAGS="-DWOLFSSL_USER_IO -Os" CPPFLAGS="-I./"
# armv7-eabihf-glibc    ./configure --host=arm-linux CC=arm-linux-gcc LD=arm-linux-ld AR=arm-linux-ar RANLIB=arm-linux-ranlib CFLAGS="-DWOLFSSL_USER_IO -Os" CPPFLAGS="-I./"