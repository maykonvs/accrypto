Projeto para teste comparativo de performance entre os métodos de criptografia e hash utilizados pelos blockchains genéricos mais populares.

## Instalando o wolfssl

Os algoritmos utilizados para comparação de performance entre os métodos de criptotrafia e hash são os disponíveis na biblioteca wolfssl. Portanto é necessário instalá-la, processo que é descrito em https://www.wolfssl.com/docs/wolfssl-manual/ch2/. Porém algumas configurações diferentes das padrões são necessárias, então siga os passos descritos na seção do sistema operacional que será utilizado:

### macOS e Linux

1. Baixar o wolfssl:
    1. http://wolfssl.com/wolfSSL/download/downloadForm.php;
    1. Não precisa preencher o formulário, só escolher o pacote do wolfssl e clicar em "Download";
1. Descompacte o arquivo baixado em uma pasta para bibliotecas, ex: libs:
1. Navegue até a pasta;
1. Configure a compilação do wolfssl habilitando as curvas customizadas do ECC, o koblitz (secp256k1) e a encriptação ECC;
    1. `./configure --enable-ecccustcurves --enable-eccencrypt CFLAGS=-DHAVE_ECC_KOBLITZ`
1. Instale o wolfssl:
    1. `make install`;
 
### Windows

Ainda não testei o sistema no Windows portanto não tenho os passos de instalação nele.

## Compilando e rodando os algoritmos de teste

Para compilar o rodar os algoritmos de teste basta seguir os passos para o sistema operacional que está sendo utilizado ou os passos para criar uma imagem e um container docker.

### macOS e Linux

```shell
gcc <source.c> -lwolfssl -o <output.out>
./<output.out>
```

### Windows

Ainda não testei este projeto no Windows, portanto não tenho o passo-a-passo para ele.

### Docker

Para o docker primeiro é necessário construir as imagens e os containers:

```shell
docker build --tag <tag_img:tag_ver> .
docker run -dit -v "$PWD":/usr/local/src/accrypto --name <container_name> <tag_img:tag_ver>
```

Após isto é possível anexar um shell no container e então compilar os algoritmos. Para compilar os algoritmos de teste de hash (sha*.c) rode os comandos abaixo.

```shell
arm-linux-gnueabihf-gcc --static <source.c> -lwolfssl -o <output.out>
```

Para compilar os algoritmos de encriptação (ecc*.c e rsa*.c) rode os comandos abaixo.

```shell
arm-linux-gnueabihf-gcc --static <source.c> -lwolfssl -lm -lpthread -o <output.out>
```

Após a compilação é possível rodar as simulações utilizando o gem5:

```shell
../gem5/build/ARM/gem5.opt ../gem5/configs/example/arm/starter_se.py --cpu="minor" <output.out>
```

Para fazer a simulação pegando os dados de benchmarkÇ

```shell
../gem5/build/ARM/gem5.opt -d se_results/<output> ../gem5/configs/example/arm/starter_se.py --cpu="minor" <output.out>
```

## Gerando as chaves

As chaves de (des)encriptação estão comitadas no repositório, porém é possível gerar novas chaves para fazer os testes dos algoritmos. Eu ainda não fiz uma forma automática de substituição das chaves e nem uma forma de passá-las por parâmetro, portanto o processo é manual, e acusará modificações nos arquivos do repositório, logo, após os testes, desfaça as modificações no git (`git checkout .`).

Está no planejamento fazer comandos de gerenciamento que possam gerar chaves e também fazer uma forma de passar por parâmetros de compilação quais chaves se deseja utilizar nos testes.

### Formato das chaves

É mostrado abaixo como gerar as chaves no formato "pem" e no formato "der", porém é necessário apenas o "der", que é de onde os bytes são pegos para serem colocados nos arrays dos headers (para mais detalhes veja a seção Headers). As chaves "pem" ficam para testes de outros algoritmos com outras bibliotecas, que utilizem esse formato.

### RSA

Os comandos abaixo geram as chaves no formato "pem".

```shell
openssl genrsa -out private.pem 512
openssl rsa -in private.pem -outform PEM -pubout -out public.pem
```

Para convertê-las para o formato "der" basta rodar os comandos abaixo substituindo os nomes dos arquivos pelos gerados nos comandos anteriores.

```shell
openssl rsa -inform pem -in private.pem -outform der -out private.der
openssl rsa -inform pem -pubin -in public.pem -outform der -pubout -out public.der
```

### ECC

Os comandos abaixo geram as chaves no formato "pem".

```shell
openssl ecparam -name secp256k1 -genkey -out private.pem
openssl ec -in private.pem -pubout -out public.pem
```

Para convertê-las para o formato "der" basta rodar os comandos abaixo substituindo os nomes dos arquivos pelos gerados nos comandos anteriores.

```shell
openssl ec -inform pem -in private.pem -outform der -out private.der
openssl ec -inform pem -pubin -in public.pem -outform der -pubout -out public.der
```

É importante observar que no caso do ECC são necessárias chaves públicas e privadas tanto para o elemento que está enviando a mensagem quanto para o elemento que está recebendo a mensagem, totalizando assim 4 chaves.

### Headers

Apenas gerar os arquivos chaves "der" não é o suficiente para os algoritmos passarem a utilizá-los. Agora é preciso pegar os bytes deles através de qualquer hex-editor e então copiá-los e colocá-los nos arrays específicos dentro dos headers (rsa_keys.h para os algoritmos de rsa e ecc_keys.h para os algoritmos de ecc).

Este processo é feito manualmente.

### Verificação das chaves

Para verificar se as chaves são válidas pode-se compilar e rodar os testes rsa_keys_test.c e ecc_keys_test.c.
