/*
 * Testa as chaves pública e privada do rsa encriptando uma mensagem com a chave pública e 
 * já desencriptando na sequencia com a chave privada, validando assim as mesmas.
 */

#include <stdio.h>

#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/asn_public.h>
#include <wolfssl/wolfcrypt/ecc.h>

#include "payload_padded.h"
#include "ecc_keys.h"

int main()
{
    // Encriptando
    ecc_key eccPublicKeySender, eccPublicKeyReceiver, eccPrivateKeySender, eccPrivateKeyReceiver;
    word32 idx;
    
    int ret;
    ret = wc_ecc_init(&eccPrivateKeySender);
    ret |= wc_ecc_init(&eccPublicKeyReceiver);

    if(ret != 0)
    {
        printf("Inicialização das chaves de encriptacao falhou: %d\n", ret);
        return -1;
    }

    ret = wc_ecc_set_curve(&eccPrivateKeySender, 32, ECC_SECP256K1);
    ret |= wc_ecc_set_curve(&eccPublicKeyReceiver, 32, ECC_SECP256K1);

    if(ret != 0)
    {
        printf("Erro ao setar a curva nas chaves de encriptacao: %d\n", ret);
        return -1;
    }

    idx = 0;
    ret = wc_EccPrivateKeyDecode(ecc_private_key_sender, &idx, &eccPrivateKeySender, sizeof(ecc_private_key_sender));
    idx = 0;
    ret |= wc_EccPublicKeyDecode(ecc_public_key_receiver, &idx, &eccPublicKeyReceiver, sizeof(ecc_public_key_receiver));

    if(ret != 0)
    {
        printf("Erro decodificando as chaves de encriptacao: %d\n", ret);
        return -1;
    }

    RNG encryptRng;
    wc_InitRng(&encryptRng);
    wc_ecc_set_rng(&eccPrivateKeySender, &encryptRng);

    byte out[128];
    word32 outSz = sizeof(out);
    
    ret = wc_ecc_encrypt(&eccPrivateKeySender, &eccPublicKeyReceiver, payload, sizeof(payload), out, &outSz, NULL);

    if(ret != 0)
    {
        printf("Erro ao encriptar: %d\n", ret);
        return -1;
    }

    // Desencriptando
    ret = wc_ecc_init(&eccPrivateKeyReceiver);
    ret |= wc_ecc_init(&eccPublicKeySender);

    if(ret != 0)
    {
        printf("Inicialização das chaves de desencriptacao falhou: %d\n", ret);
        return -1;
    }

    ret = wc_ecc_set_curve(&eccPrivateKeyReceiver, 32, ECC_SECP256K1);
    ret |= wc_ecc_set_curve(&eccPublicKeySender, 32, ECC_SECP256K1);

    if(ret != 0)
    {
        printf("Erro ao setar a curva nas chaves de desencriptacao: %d\n", ret);
        return -1;
    }

    idx = 0;
    ret = wc_EccPrivateKeyDecode(ecc_private_key_receiver, &idx, &eccPrivateKeyReceiver, sizeof(ecc_private_key_receiver));
    idx = 0;
    ret = wc_EccPublicKeyDecode(ecc_public_key_sender, &idx, &eccPublicKeySender, sizeof(ecc_public_key_sender));

    if(ret != 0)
    {
        printf("Erro decodificando as chaves de desencriptacao: %d\n", ret);
        return -1;
    }

    RNG decryptRng;
    wc_InitRng(&decryptRng);
    wc_ecc_set_rng(&eccPrivateKeyReceiver, &decryptRng);

    byte plain[128];
    word32 plainSz = sizeof(plain);

    ret = wc_ecc_decrypt(&eccPrivateKeyReceiver, &eccPublicKeySender, out, outSz, plain, &plainSz, NULL);

    if(ret != 0)
    {
        printf("Erro ao desencriptar: %d\n", ret);
        return -1;
    }

    // Verificando se as mensagens de entrada e saída são iguais.

    if(plainSz != sizeof(payload))
    {
        printf("O tamanho das mensagens não bate: %d/%lu\n", plainSz, sizeof(payload));
        return -1;
    }

    int i;
    for(i = 0; i < plainSz; i++)
    {
        if(plain[i] != payload[i])
        {
            printf("O byte %d da mensagem não bate: %d/%d\n", i, plain[i], payload[i]);
            return -1;
        }
    }

    printf("A mensagem desencriptada é igual a original!\n");
    return 0;
}
