#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/types.h>

// Este header existe para que todos os códigos de teste compartilhem o mesmo payload, mantendo a facilidade de
// modificar ele sem precisar editar todos os códigos de teste.
// Não é utilizado um extern aqui com um arquivo c declarando a variável pois eu preciso determinar o tamanho
// deste buffer. Como não quero colocar overheads com uma função getter eu declarei a variável inteira aqui.
// Como cada teste possui a apenas um arquivo c principal que utiliza este header isto não terá problema nenhum.

// {"data": "Exemplo de payload!"}
// Serializado utilizando protobuf através da ferramento online http://yura415.github.io/js-protobuf-encode-decode/

byte payload[] = 
{
    0x0A, 0x13, 0x45, 0x78, 0x65, 
    0x6D, 0x70, 0x6C, 0x6F, 0x20, 
    0x64, 0x65, 0x20, 0x70, 0x61, 
    0x79, 0x6C, 0x6F, 0x61, 0x64, 
    0x21
};
