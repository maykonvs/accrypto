/*
 * Testa as chaves pública e privada do rsa encriptando uma mensagem com a chave pública e 
 * já desencriptando na sequencia com a chave privada, validando assim as mesmas.
 */

#include <stdio.h>

#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/rsa.h>

#include "payload.h"
#include "keys.h"

int main()
{
    // Encriptando
    RsaKey rsaPublicKey;
    word32 idx = 0;
    
    wc_InitRsaKey(&rsaPublicKey, NULL);
    int validKey = wc_RsaPublicKeyDecode(rsa_public_key, &idx, &rsaPublicKey, sizeof(rsa_public_key));

    if(validKey != 0)
    {
        printf("Erro decodificando a chave: %d", validKey);
        return -1;
    }

    byte out[128];

    RNG encryptRng;
    wc_InitRng(&encryptRng);
    
    int outLen = wc_RsaPublicEncrypt(payload, sizeof(payload), out, sizeof(out), &rsaPublicKey, &encryptRng);

    if(outLen < 0)
    {
        printf("Erro ao encriptar: %d\n", outLen);
        return -1;
    }

    // Desencriptando
    RsaKey rsaPrivateKey;
    idx = 0;

    RNG decryptRng;
    wc_InitRng(&decryptRng);
    
    wc_InitRsaKey(&rsaPrivateKey, NULL);
    wc_RsaSetRNG(&rsaPrivateKey, &decryptRng);
    validKey = wc_RsaPrivateKeyDecode(rsa_private_key, &idx, &rsaPrivateKey, sizeof(rsa_private_key));

    if(validKey != 0)
    {
        printf("Erro decodificando a chave: %d", validKey);
        return -1;
    }

    byte plain[128];
    int plainSz = wc_RsaPrivateDecrypt(out, outLen, plain, sizeof(plain), &rsaPrivateKey);

    if(plainSz < 0)
    {
        printf("Erro ao desencriptar: %d\n", plainSz);
        return -1;
    }

    // Verificando se as mensagens de entrada e saída são iguais.

    if(plainSz != sizeof(payload))
    {
        printf("O tamanho das mensagens não bate: %d/%lu\n", plainSz, sizeof(payload));
        return -1;
    }

    int i;
    for(i = 0; i < plainSz; i++)
    {
        if(plain[i] != payload[i])
        {
            printf("O byte %d da mensagem não bate: %d/%d\n", i, plain[i], payload[i]);
            return -1;
        }
    }

    printf("A mensagem desencriptada é igual a original!\n");
    return 0;
}