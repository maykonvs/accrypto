#include <stdio.h>

#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha3.h>

#include "payload.h"

int main()
{
    byte shaSum[SHA3_256_DIGEST_SIZE];   
    int i;   

    Sha3 sha;
    wc_InitSha3_256(&sha, NULL, 0);
    wc_Sha3_256_Update(&sha, payload, sizeof(payload));
    wc_Sha3_256_Final(&sha, shaSum);

    printf("Hash SHA3-256:\n");

    for(i = 0; i < SHA3_256_DIGEST_SIZE; i++)
        printf("[%02d] - %d\n", i, shaSum[i]);
}
