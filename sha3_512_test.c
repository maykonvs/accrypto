#include <stdio.h>

#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha3.h>

#include "payload.h"

int main()
{
    byte shaSum[SHA3_512_DIGEST_SIZE];   
    int i;   

    Sha3 sha;
    wc_InitSha3_512(&sha, NULL, 0);
    wc_Sha3_512_Update(&sha, payload, sizeof(payload));
    wc_Sha3_512_Final(&sha, shaSum);

    printf("Hash SHA3-512:\n");

    for(i = 0; i < SHA3_512_DIGEST_SIZE; i++)
        printf("[%02d] - %d\n", i, shaSum[i]);
}
