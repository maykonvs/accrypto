#include <stdio.h>

#include <wolfssl/options.h>
#include <wolfssl/wolfcrypt/sha512.h>

#include "payload.h"

int main()
{
    byte shaSum[SHA512_DIGEST_SIZE];   
    int i;   

    Sha512 sha;
    wc_InitSha512(&sha);
    wc_Sha512Update(&sha, payload, sizeof(payload));
    wc_Sha512Final(&sha, shaSum);

    printf("Hash SHA512:\n");

    for(i = 0; i < SHA512_DIGEST_SIZE; i++)
        printf("[%02d] - %d\n", i, shaSum[i]);
}
